# The Heinrichs Website

Website in progress.

## Installation

Install npm packages:

`npm install`

## Running

You can simply run gulp to get a running version in a browser.
This is done via the builtin PHP server, that is proxied in broser-sync.
The default gulp task starts this and watches js/css files aswell as PHP templates.

`gulp`
